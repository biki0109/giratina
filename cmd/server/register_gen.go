// IM AUTO GENERATED, BUT CAN BE OVERRIDDEN

package main

import (
	"git.begroup.team/platform-transport/giratina/internal/services"
	"git.begroup.team/platform-transport/giratina/internal/stores"
	"git.begroup.team/platform-transport/giratina/pb"
	"honnef.co/go/tools/config"
)

func registerService(cfg *config.Config) pb.GiratinaServer {

	mainStore := stores.NewMainStore()

	return services.New(cfg, mainStore)
}
