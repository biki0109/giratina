package client

import (
	"context"

	"git.begroup.team/platform-core/kitchen/l"
	"git.begroup.team/platform-transport/giratina/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
)

type GiratinaClient struct {
	pb.GiratinaClient
}

func NewGiratinaClient(address string) *GiratinaClient {
	conn, err := grpc.DialContext(context.Background(), address,
		grpc.WithInsecure(),
		grpc.WithBalancerName(roundrobin.Name),
	)

	if err != nil {
		ll.Fatal("Failed to dial Giratina service", l.Error(err))
	}

	c := pb.NewGiratinaClient(conn)

	return &GiratinaClient{c}
}
